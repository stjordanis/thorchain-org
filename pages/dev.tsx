import PapersSection from 'components/Home/PapersSection'

const Dev = () => (
  <>
    {/* <div className="container-fluid lgrey-bg min-h-30v pt-20vh pb-10vh">
      <div className="container ecosystem-container">
        <div className="row">
          <div className="col-6">
            <h1>DEVELOPERS</h1>
            <h5>Learn how to connect to THORChain, as well as building on it.</h5>
            <br />
            <div className="col-6 py-1" style={{ marginBottom: '50px', textAlign: 'center' }}>
              <a href="https://docs.thorchain.org" target="_blank">
                <button className="btn btn-thor py-2" style={{ height: '45px', marginTop: '20px' }}>
                  DEVELOPER DOCS
            </button>
              </a>
            </div>
            <div className="col-6 py-1" style={{ marginBottom: '50px', textAlign: 'center' }}>
              <a href="http://midgard-explorer.herokuapp.com/" target="_blank">
                <button className="btn btn-thor py-2" style={{ height: '45px', marginTop: '20px' }}>
                  MIDGARD EXPLORER
            </button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div> */}

<div className="container-fluid lgrey-bg min-h-30v pt-20vh">
      <div className="container ecosystem-container">
        <div className="row">
          <div className="col-md-12 col-lg-6">
            <h1>DEVELOPERS</h1>
            <h5>Learn how to connect to THORChain, as well as building on it.</h5>
            <br />
            <div className="col-6 py-1" style={{ marginBottom: '50px', textAlign: 'center' }}>
              <a href="https://docs.thorchain.org" target="_blank">
                <button className="btn btn-thor py-2" style={{ height: '45px', marginTop: '20px' }}>
                  DEVELOPER DOCS
            </button>
              </a>
            </div>
          </div>
          <div className="col-md-12 col-lg-6">
            <div className="container  text-center" style={{ justifyContent: 'center' }}>
              <a href="http://midgard-explorer.herokuapp.com/" target="_blank">
                <div className="thorchain-component" style={{ display: 'inline-block' }}>
                  <h4>
                    <span className="pr-10px"></span>
                  MIDGARD EXPLORER
                </h4>
                  <div
                    className="bg-thorchain-gradient-center w-100p"
                    style={{ height: '1px', marginBottom: '10px' }}
                  />
                  <p>Explore Midgard API Endpoints</p>
                  <p style={{ color: '#00ccff' }}>midgard-explorer.herokuapp.com/</p>
                </div>
              </a>
              <a href="https://docs.google.com/document/u/1/d/1ZoJQKvyATQekFbWMk_rqX96K9BSmCArh9e-A_g66wDQ/mobilebasic#heading=h.xymrivoqt1ra" target="_blank">
                <div className="thorchain-component" style={{ display: 'inline-block' }}>
                  <h4>
                    <span className="pr-10px"></span>
                  MIDGARD API SPEC
                </h4>
                  <div
                    className="bg-thorchain-gradient-center w-100p"
                    style={{ height: '1px', marginBottom: '10px' }}
                  />
                  <p>Explore Midgard's design spec</p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="container-fluid lgrey-bg min-h-60v pt-10vh">
      <div className="container ecosystem-container">
        <div className="row component-row my-30 text-left">

          <div className="row">
            <div className="col-12">
              <h2>REPOS</h2>
              <br />
              <p className="desc-paragraph">
                THORChain code is on several repositories.
                <br />
                <br />
              </p>
            </div>
          </div>
          <div className="container  text-center" style={{ justifyContent: 'center' }}>
            <a href="https://gitlab.com/thorchain/thornode" target="_blank">
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  <span className="pr-10px"></span>THORNODE
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>The main repo for THORNode</p>
                <p style={{ color: '#00ccff' }}>gitlab.com/thorchain/thornode</p>
              </div>
            </a>
            <a
              href="https://gitlab.com/thorchain/midgard"
              target="_blank"
            >
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  <span className="pr-10px"></span>
                  MIDGARD
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>
                  Midgard API Repo
                </p>
                <p style={{ color: '#00ccff' }}>gitlab.com/thorchain/midgard</p>
              </div>
            </a>
            <a href="https://github.com/thorchain/asgardex-electron" target="_blank">
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  <span className="pr-10px"></span> ASGARDEX Desktop App
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>
                  ASGARDEX Desktop Client
                </p>
                <p style={{ color: '#00ccff' }}>github.com/thorchain/asgardex-electron</p>
              </div>
            </a>
            <a href="https://gitlab.com/thorchain/asgardex-common" target="_blank">
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  <span className="pr-10px"></span> ASGARDEX NPM Packages
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>
                  ASGARDEX NPM Packages to accelerate rate of integration into wallets.
                </p>
                <p style={{ color: '#00ccff' }}>gitlab.com/thorchain/asgardex-common</p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>

    <PapersSection />
  </>
)

export default Dev