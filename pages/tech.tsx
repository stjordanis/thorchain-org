import TechSection from 'components/Home/TechSection'

const Tech = () => (
  <>
    <TechSection />
  </>
)

export default Tech
