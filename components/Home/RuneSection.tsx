import React, { FC } from 'react'

export const RuneSection: FC = () => (
  <div className="container-fluid min-h-60v pb-10vh" id="RUNE">
    <div className="container">
      <div className="row text-center  pt-10vh">
        <div className="col-12 pb-50">
          <h2 style={{ letterSpacing: '2.75px' }}>RUNE</h2>
          <div
            className="bg-thorchain-gradient-center"
            style={{ margin: '20px auto', height: '2px', width: '75%' }}
          />
          <h5>A FIXED-SUPPLY NETWORK ASSET</h5>
        </div>
      </div>
      <div className="row pt-5vh">
        <div className="col-lg-8 col-md-12">
          <div className="col-12 pitch-text">
            <h5 className="mt-5">ON-CHAIN LIQUIDITY</h5>
            <ul>
              <li>All pools contain RUNE at a 50:50 ratio</li>
              <li>A single settlement asset means any two pools can be linked</li>
              <li>Only pooled capital is secured, simplifying the economic security model</li>
              <li>Pools earn 1/3rd of the System Income</li>
            </ul>
            <h5 className="mt-5">NETWORK SECURITY</h5>
            <ul>
              <li>All nodes bond RUNE at a 2:1 ratio to pooled RUNE</li>
              <li>Highest bonded nodes are churned-in to drive bonding rates</li>
              <li>Nodes are slashed if malicious behaviour is detected</li>
              <li>Nodes earn 2/3rds of the System Income</li>
            </ul>
          </div>
          <div className="col-12 py-1 mx-1">
                <h6>
                  <a
                    href="https://docs.google.com/spreadsheets/d/1e5A7TaV6CZtdVqlOSuXSSY7UYiRW9yzd1ST6QTZNqLw/edit?usp=sharing"
                    target="_blank"
                    style={{ color: '#50E3C2' }}
                  >
                    RUNE EMISSION SCHEDULE
                  </a>
                </h6>
              </div>
        </div>
        <div className="col-lg-4 text-center my-5">
          <div className="container">
            <div className="row">
              <div className="col mx-3 py-5">
                <img className="img-fluid" src={require('assets/img/rune-token.png')} />
              </div>
              <div className="col-12 py-1">
                <a href="https://www.binance.com?ref=QWHONFUV" target="_blank">
                  <button
                    className="btn btn-thor py-2"
                    style={{ height: '45px', marginTop: '20px' }}
                  >
                    BUY ON BINANCE
                  </button>
                </a>
              </div>
              <div className="col-12" style={{ marginTop: '20px' }}>
                <h5>
                  <a
                    href="https://coinmarketcap.com/currencies/thorchain/"
                    target="_blank"
                    style={{ color: '#00ccff', fontSize: '18px' }}
                  >
                    VIEW ON COINMARKETCAP
                  </a>
                </h5>
              </div>
              <div className="col-12" style={{ marginTop: '20px' }}>
                <h5>
                  <a
                    href="https://www.coingecko.com/en/coins/thorchain"
                    target="_blank"
                    style={{ color: '#50E3C2', fontSize: '18px' }}
                  >
                    VIEW ON COINGECKO
                  </a>
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default RuneSection
